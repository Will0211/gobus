gobus.factory('SessionService', ['$http', function($http) {
    return {
        setData: function(key, value) {
            return localStorage.setItem(key, JSON.stringify(value));
        },
        getData: function(key) {
            return JSON.parse(localStorage.getItem(key));
        },
        destroyData: function(key) {
            return localStorage.removeItem(key);
        },
        destroyAll: function() {
            return localStorage.clear();
        },
    };
}])
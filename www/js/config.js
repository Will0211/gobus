gobus.config(function($stateProvider, $urlRouterProvider, $logProvider) {
    $stateProvider
        .state('login', {
            url: '/login/:flag',
            templateUrl: 'templates/login.html',
            controller: 'LoginController'
        })
        .state('register', {
            url: '/register',
            templateUrl: 'templates/register.html',
            controller: 'RegisterController'

        })
        .state('resetPassword', {
            url: '/resetPassword',
            templateUrl: 'templates/resetPassword.html',
            controller: 'ResetPassController'
        })
        .state('app', {
            url: '/app',
            templateUrl: 'templates/menu.html',
            controller: 'AppController'
        })
        .state('app.home', {
            url: '/home',
            views: {
                'menuContent': {
                    templateUrl: 'templates/home.html',
                    controller: 'HomeController'
                }
            }
        })
        .state('app.routes', {
            url: '/routes/:zone',
            views: {
                'menuContent': {
                    templateUrl: 'templates/routes.html',
                    controller: 'RoutesController'
                }
            }
        })
        .state('app.route', {
            url: '/route/:route',
            views: {
                'menuContent': {
                    templateUrl: 'templates/route.html',
                    controller: 'RouteController'
                }
            }
        })
        .state('app.route_detail', {
            url: '/route_detail/:route',
            views: {
                'menuContent': {
                    templateUrl: 'templates/route_detail.html',
                    controller: 'RouteDetailController'
                }
            }
        })
        .state('app.search', {
            url: '/search',
            views: {
                'menuContent': {
                    templateUrl: 'templates/search.html',
                    controller: 'SearchController'
                }
            }
        })
        .state('app.place_interest', {
            url: '/place_interest',
            views: {
                'menuContent': {
                    templateUrl: 'templates/place_interest.html',
                    controller: 'PlaceInterestController'
                }
            }
        })
        .state('app.nearby_routes', {
            url: '/nearby_routes',
            views: {
                'menuContent': {
                    templateUrl: 'templates/nearby_routes.html',
                    controller: 'NearbyRoutesController'
                }
            }
        })
        .state('app.alerts', {
            url: '/alerts',
            views: {
                'menuContent': {
                    templateUrl: 'templates/alerts.html',
                    controller: 'AlertsController'
                }
            }
        })
        .state('app.zones', {
            url: '/zones',
            views: {
                'menuContent': {
                    templateUrl: 'templates/zones.html',
                    controller: 'ZonesController'
                }
            }
        })
        .state('app.results', {
            url: '/results/:results',
            views: {
                'menuContent': {
                    templateUrl: 'templates/results.html',
                    controller: 'ResultsController'
                }
            }
        })
        .state('app.profile', {
            url: '/profile',
            views: {
                'menuContent': {
                    templateUrl: 'templates/profile.html',
                    controller: 'ProfileController'
                }
            }
        })
        .state('splash', {
            url: '/splash', 
            templateUrl: 'templates/splash.html',
            controller: 'splashController'
        })
        .state('app.ranking', {
            url: '/ranking',
            views: {
                'menuContent': {
                    templateUrl: 'templates/ranking.html',
                    controller: 'RankingController'
                }
            }
        });

    $urlRouterProvider.otherwise('/splash');
    $logProvider.debugEnabled(true);
});
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var gobus = angular.module('GoBus', ['ionic', 'ngCordova', 'ngCordovaOauth', 'ionic.ion.showWhen', 'fileLogger', 'autocomplete'])
gobus.factory('RecoveryService', RecoveryService);
gobus.run(function($ionicPlatform, $rootScope, $state, SessionService, $ionicPopup, $cordovaToast) {
    $ionicPlatform.ready(function() {

        //var googleanalyticsApp = angular.module('googleanalytics', ['ionic'])

       
                if(typeof window.ga !== "undefined") {
                    window.ga.startTrackerWithId("UA-79258652-2");
                    window.ga.trackEvent('Launch', 'Start');
                    window.ga.trackView('Login');
                    //window.ga.setUserId('my-user-id')
                    console.log("Google Analytics Available!!");
                } else {
                    console.log("Google Analytics Unavailable");
                }


        navigator.geolocation.getCurrentPosition(function(pos) {}, function(err) {

        });


        if (window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }


        cordova.plugins.diagnostic.requestCameraAuthorization(function(status) {}, function(error) {});

        $ionicPlatform.registerBackButtonAction(function(event) {
            switch ($state.current.name) {
                case "app.home":
                    event.preventDefault();
                    event.stopPropagation();
                    if (true) { // your check here
                          $ionicPopup.confirm({
                            title: 'Cuidado',
                            template: 'Estás a punto de cerrar la aplicación por completo, si lo que quieres es minimizar presiona el botón de Home de tu dispositivo',
                            cancelText: 'No quiero cerrar',
                            okText: 'Cerrar'
                          }).then(function(res) {
                            if (res) {
                              ionic.Platform.exitApp();
                            }
                          })
                      }
                    break;
                case "login":
                    navigator.app.exitApp();
                    break;
                case "app.route":
                    //$rootScope.abConnection.close();
                    navigator.app.backHistory();
                    break;
                default:
                    if ($rootScope.map != undefined) {
                        $rootScope.map._onResize();
                    }
                    if ($rootScope.mapRoute != undefined) {
                        $rootScope.mapRoute._onResize();
                    }

                    navigator.app.backHistory();
                    break;
            }

        }, 100);


        if (SessionService.getData('user') != undefined && SessionService.getData('user').status == "ok") {
            $rootScope.userLogged = SessionService.getData('user');
            $state.go('app.home');
        }
        else
        {
            $state.go('login');
        }


    });
});




gobus.controller('AnalyticsController', function($scope) {
    if(typeof window.ga !== "undefined") { 
        window.ga.startTrackerWithId("UA-79258652-2");
        window.ga.trackView('home');
        window.ga.trackView('Login'); 
        console.log("Google Analytics Controller Available!!");
    } else {
        console.log("Google Analytics Controller Unavailable");
    }
 
    $scope.initEvent = function() {
        if(typeof window.ga !== "undefined") { window.ga.trackEvent("Impresiones1", "Vistas1", "Home1", 25); }
    }
});

/**
 *Objeto para guardar los datos de el usuario en sesión
 */
function User(id_user, registration_type, status, email, img_profile, name) {
    this.id_usuario = id_user;
    this.status = status;
    this.registration_type = registration_type;
    this.email = email;
    this.img_profile = img_profile;
    this.name = name;
}


Array.prototype.getObjectById = function(field, value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][field] == value) {
            return i
        }
    }
    return -1;
};
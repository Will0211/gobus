gobus.controller('ResultsController', function($scope, $rootScope, $state, $stateParams, $location, SessionService) {
    $scope.results = JSON.parse($stateParams.results);
    $(".content-page").css('height', $(window).height());

    $scope.viewRoute = function(result) {
        $state.go('app.route', {
            route: JSON.stringify(result)
        });
    };


    $scope.getImageStop = function(url) {
        if (url != null) {
            return IP + "uploads/img_ruta/" + url;
        } else {
            return "img/icons/not-found.png"
        }
    };
});
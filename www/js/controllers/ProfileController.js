gobus.controller('ProfileController', function($scope, $rootScope, $ionicModal, SessionService, $ionicPopup, $http, $cordovaToast, $cordovaImagePicker, $cordovaFileTransfer) {
    $scope.user = $rootScope.userLogged;
    $scope.user.current_password = "";
    $scope.user.password = "";
    $scope.user.match_password = "";
    $scope.user.change_password = false;
    $scope.changeFile = false;
    $scope.distance = 0;

    $scope.init = function() {

        var template = "";
        navigator.userAgent.match(/iPhone | iPad | iPod/i) ? template = '<ion-spinner style="margin-left:40%;" icon="ios"></ion-spinner>' : template = '<ion-spinner style="margin-left:40%;" icon="android"></ion-spinner>';

        $scope.popup = $ionicPopup.show({
            template: template,
            title: 'Cargando Perfil',
            scope: $scope,
        });

        $rootScope.img_profile = IP + "uploads/img_users/" + $scope.user.img_profile;

        $http({
            method: 'GET',
            url: $rootScope.img_profile
        }).then(function successCallback(response) {
                $scope.popup.close();
            },
            function errorCallback(response) {
                $rootScope.img_profile = $scope.user.img_profile;
                $http({
                    method: 'GET',
                    url: $rootScope.img_profile
                }).then(function successCallback(response) {
                        $scope.popup.close();
                    },
                    function errorCallback(response) {
                        $scope.popup.close();
                        $rootScope.img_profile = "img/icons/placeholder_user.png"
                    });
            });




        $.ajax({
            type: 'POST',
            url: IP + 'Ws/get_km/1',
            data: { id_usuario: $rootScope.userLogged.id_usuario },
            timeout: 5000,
            success: function(response) {
                $scope.distance = response;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $scope.distance = 0;
            },
            dataType: 'JSON'
        });


    };

    $scope.changePassword = function() {
        $scope.user.current_password = "";
        $scope.user.password = "";
        $scope.user.match_password = "";
    };

    $scope.changeImage = function() {
        cordova.plugins.diagnostic.requestCameraAuthorization(function(status) {
            if (status == cordova.plugins.diagnostic.permissionStatus.GRANTED || status == cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS) {
                $cordovaImagePicker.getPictures({
                        maximumImagesCount: 1,
                        width: 800,
                        height: 800,
                        quality: 80
                    })
                    .then(function(results) {
                        if (results.length > 0) {
                            $rootScope.img_profile = results[0];
                            $scope.changeFile = true;
                        }
                    }, function(error) {});
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: 'Importante',
                    template: 'Para poder cambiar su imagen de perfil, debe permitir a la aplicación leer los archivos de su equipo.'
                });
            }
        }, function(error) {});
    }

    $scope.updateUser = function(params) {

        if ($scope.changeFile) {
            $cordovaFileTransfer.upload(IP + "Ws/upload_image", $rootScope.img_profile, {
                    params: $scope.user
                })
                .then(function(result) {
                    var response = JSON.parse(result.response);
                    if (response.status == "ok") {
                        var resp = JSON.parse(result.response);
                        var user = new User(resp.user.id_usuario, resp.user.registration_type, resp.status, resp.user.email, resp.user.img_profile, resp.user.name);
                        SessionService.destroyData('user');
                        SessionService.setData('user', user);
                        $rootScope.userLogged = user;
                        $rootScope.image_profile = IP + "uploads/img_users/" + resp.user.img_profile;

                        $scope.sendData();
                    } else {
                        $cordovaToast.show(response.response, "long", "bottom").then(function(success) {}, function(error) {});
                    }
                }, function(err) {
                    if (err.code == 1) {
                        $scope.sendData();
                    }
                }, function(progress) {
                    //console.log(progress)
                });
        } else {
            $scope.sendData();
        }

    };

    $scope.sendData = function() {
        $.ajax({
            type: 'POST',
            url: IP + 'Ws/update_user',
            data: $scope.user,
            timeout: 5000,
            success: function(response) {
                if (response.status = 'ok') {
                    var user = new User(response.user.id_usuario, response.user.registration_type, response.status, response.user.email, response.user.img_profile, response.user.name);
                    SessionService.destroyData('user');
                    SessionService.setData('user', user);
                    $rootScope.userLogged = user;
                    $cordovaToast.show("Sus datos fueron actualizados", "short", "bottom").then(function(success) {
                        $scope.changeFile = false;
                    }, function(error) {});
                } else {
                    $cordovaToast.show(response.response, "long", "bottom").then(function(success) {}, function(error) {});
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $scope.popup.close();
                $scope.showErrorMessages(jqXHR.status);
            },
            dataType: 'JSON'
        });
    }

    if(typeof window.ga !== "undefined") {
                    window.ga.startTrackerWithId("UA-79258652-2");
                    window.ga.trackEvent('PerfilEvent', 'Visit');
                    window.ga.trackView('Perfil');
                    console.log("Google Analytics PERFIL Available!!");
                } else {
                    console.log("Google Analytics PERFIL Unavailable");
                }

    $scope.showErrorMessages = function(response) {
        var message = ""
        switch (response.status) {
            case 500:
                message = "Ocurrio un erro al intentar procesar su solicitud, intentelo de nuevo mas tarde o contacte al administrador del sistema";
                break;
            case 408:
            case 504:
                message = "El servidor tardo demasiado en responder";
                break;
            case 503:
                message = "El servicio no se encuentra disponible por el momento, intentelo de nuevo mas tarde o conctacte al administrador del sistema";
                break;
            case 204:
                message = "No se encontro la respuesta solicitada, intentelo nuevamente";
            default:
                message = "Ocurio un error consulte al administrador";

        }

        $cordovaToast.show(message, "long", "bottom").then(function(success) {}, function(error) {});
    }

    angular.element(document).ready(function() {
        $scope.init();
    });
});
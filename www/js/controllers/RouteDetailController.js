gobus.controller('RouteDetailController', function($scope, $ionicPlatform, $rootScope, $state, $stateParams, $http, $ionicPopup, $cordovaToast, SessionService, $ionicPopover, $ionicModal) {
    $(".content-page").css('height', $(window).height());
    $scope.actionDestiny = true;
    $scope.actionComments = false;

    $scope.user_session = $rootScope.userLogged;

    $scope.route = JSON.parse($stateParams.route);
    $(".content-page").css('height', $(window).height());

    $scope.destinies = [];
    $scope.comments = [];
    $scope.imageContent = {};

    $ionicModal.fromTemplateUrl('image', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.zoomImageModal = modal;
    });

    $scope.changeView = function(id) {
        if (id == 1) {
            $scope.actionDestiny = true;
            $scope.actionComments = false;
        } else {
            $scope.actionDestiny = false;
            $scope.actionComments = true;
            $scope.getComments();
        }
    }

    $scope.getComments = function() {
        $http({
            method: 'POST',
            url: IP + "Ws/get_comments/" + $scope.route.id_ruta,
        }).then(function successCallback(response) {
            angular.forEach(response.data.comments, function(value, key) {
                value.date = $scope.getDateByStamp(value.timestamp);
            });
            $scope.comments = response.data.comments;
        }, function errorCallback(response) {
            $cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
        });
    };

    $scope.init = function() {
        var template = "";
        navigator.userAgent.match(/iPhone | iPad | iPod/i) ? template = '<ion-spinner style="margin-left:40%;" icon="ios"></ion-spinner>' : template = '<ion-spinner style="margin-left:40%;" icon="android"></ion-spinner>';

        var popup = $ionicPopup.show({
            template: template,
            title: 'Cargando ruta',
            scope: $scope,
        });


        /* Metodo para obtener los destinos */
        $http({
            method: 'POST',
            url: IP + "Ws/get_route_destinations/" + $scope.route.id_ruta,
        }).then(function successCallback(response) {
            $scope.destinies = response.data.destinos;

        }, function errorCallback(response) {
            $cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
        });

        /* Metodo para obtener los comentarios */

        $http({
            method: 'POST',
            url: IP + "Ws/get_img_ruta_by_ruta/" + $scope.route.id_ruta,
        }).then(function successCallback(response) {
            var image = "url(img/icons/not-found.png)";
            if (response.data.list_img_ruta.length > 0) {
                image = "url(" + IP + "/uploads/img_ruta/" + response.data.list_img_ruta[0].url + ")";
                $scope.imageZoom = IP + "/uploads/img_ruta/" + response.data.list_img_ruta[0].url;
            }
            $scope.imageContent = {
                "background-image": image
            };

            $scope.images = response.data.list_img_ruta;
            popup.close();
        }, function errorCallback(response) {
            $cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
        });

        $ionicPopover.fromTemplateUrl('templates/menu-comments.html', {
            scope: $scope
        }).then(function(popover) {
            $scope.popover = popover;
        });

        window.addEventListener('native.keyboardshow', function() {
            $(".comments-container").fadeOut();
        });

        window.addEventListener('native.keyboardhide', function() {
            $(".comments-container").fadeIn();
        });

    };

    $scope.sendComment = function(comment) {
        if (comment.replace(/ /g, '') == "") {
            $cordovaToast.show("debe escribir un comentario", "short", "bottom").then(function(success) {}, function(error) {});
        } else {

            var formComment = new FormData();
            formComment.append('id_ruta', $scope.route.id_ruta);
            formComment.append('id_user', $rootScope.userLogged.id_usuario);
            formComment.append('comment', comment);

            $http({
                method: 'POST',
                url: IP + "Ws/create_comment",
                data: formComment,
                headers: {
                    'Content-Type': undefined
                },
            }).then(function successCallback(response) {
                if (response.data.status == "ok") {
                    $cordovaToast.show("se guardo su comentario", "short", "bottom").then(function(success) {}, function(error) {});
                    $("#comment").val('');
                    $scope.getComments();
                } else {
                    $cordovaToast.show(response.data.response, "short", "bottom").then(function(success) {}, function(error) {});
                }
            }, function errorCallback(response) {
                $cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
            });
        }
    };

    $scope.getDateByStamp = function(stamp) {
        stamp = stamp.substring(0, stamp.indexOf(' '));
        var date = new Date(stamp);
        return date.getDate() + " de " + MONTH[date.getMonth()] + " de " + date.getFullYear();
    };

    $scope.editComment = function($event, comment) {
        $scope.openPopover($event, comment);
    };

    $scope.openPopover = function($event, comment) {
        $scope.popover.show($event);
        $scope.popover.comment = comment;
    };

    $scope.closePopover = function() {
        $scope.popover.hide();
    };

    $scope.deleteComment = function(comment) {
        $scope.closePopover();
        var confirmPopup = $ionicPopup.confirm({
            title: 'Eliminar comentario',
            template: '¿Esta seguro que desea eliminar su comentario?',
            buttons: [{
                text: 'Cancelar',
                onTap: function(e) {
                    return false;
                }
            }, {
                text: '<b>Si</b>',
                type: 'button-assertive',
                onTap: function(e) {
                    return true;
                }
            }]
        });
        confirmPopup.then(function(res) {
            if (res) {
                $http({
                    method: 'GET',
                    url: IP + "Ws/delete_comment/" + comment.id_comment,
                }).then(function successCallback(response) {
                    if (response.data.status == "ok") {
                        $cordovaToast.show("Su comentario fue eliminado", "short", "bottom").then(function(success) {}, function(error) {});
                        $scope.getComments();
                        $scope.closePopover();
                    } else {
                        $cordovaToast.show(response.data.response, "short", "bottom").then(function(success) {}, function(error) {});
                        $scope.closePopover();
                    }
                }, function errorCallback(response) {
                    $cordovaToast.show("no se pudo eliminar el comentario", "short", "bottom").then(function(success) {}, function(error) {});
                    $scope.closePopover();
                });
            }
        });
    }

    $scope.updateComment = function(comment) {
        $scope.closePopover();
        $scope.data = {}
        var confirmPopup = $ionicPopup.confirm({
            title: 'Editar comentario',
            template: '<input type="text" value="{{comment.comment}}" ng-model="data.comment_edit">',
            subTitle: "Ingrese su comentario",
            scope: $scope,
            buttons: [{
                text: 'Cancelar',
                type: 'button-assertive'
            }, {
                text: '<b>Guardar</b>',
                type: 'button-positive',
                onTap: function(e) {
                    if (!$scope.data.comment_edit) {
                        e.preventDefault();
                    } else {
                        return $scope.data.comment_edit;
                    }
                }
            }]
        });

        $scope.data.comment_edit = comment.comment;


        confirmPopup.then(function(res) {
            if (res) {
                if ($scope.data.comment_edit.replace(/ /g, '') != "") {
                    var formComment = new FormData();
                    formComment.append('id_comment', comment.id_comment);
                    formComment.append('comment', $scope.data.comment_edit);
                    $http({
                        method: 'POST',
                        url: IP + "Ws/edit_comment",
                        data: formComment,
                        headers: {
                            'Content-Type': undefined
                        },
                    }).then(function successCallback(response) {
                        if (response.data.status == "ok") {
                            $cordovaToast.show("Su comentario fue editado", "short", "bottom").then(function(success) {}, function(error) {});
                            $scope.getComments();
                        } else {
                            $cordovaToast.show(response.data.response, "short", "bottom").then(function(success) {}, function(error) {});
                        }
                    }, function errorCallback(response) {

                        $cordovaToast.show("no se pudo actualizar el usuario", "short", "bottom").then(function(success) {}, function(error) {});
                    });
                } else {

                }
            }
        });
    };

    $scope.openModal = function() {
        $scope.zoomImageModal.show();
    };

    $scope.closeModal = function() {
        $scope.zoomImageModal.hide();
    }

    angular.element(document).ready(function() {
        $scope.init();
    });


    $scope.getPathImage = function(image) {
        return IP + "/uploads/img_ruta/" + image;
    }

    $scope.getImageComment = function(image_url) {
        var imageUrl = "";
        if (image_url != null) {
            if (image_url.includes("scontent.xx.fbcdn")) {
                imageUrl = image_url;
            } else {
                imageUrl = IP + "/uploads/img_ruta/" + image_url;
            }
        } else {
            imageUrl = "img/icons/placeholder_user.png";
        }
        return imageUrl;
    };

    var MONTH = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];

})
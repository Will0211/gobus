gobus.controller('ZonesController', function($scope, $rootScope, $ionicModal, $timeout, $state, $http, $cordovaToast, $ionicPopup, SessionService) {

    $(".content-page").css('height', $(window).height());

    $scope.zones = [];
    $rootScope.positionOnMap = null;
    $scope.searchRedirect = function() {
        $state.go('app.search');
    };

    $scope.viewRoutes = function(zone) {
        $state.go('app.routes', {
            zone: JSON.stringify(zone)
        });
    };

    $scope.changeState = function(state) {
        $state.go(state);
    };


    $scope.getData = function() {
        var template = "";
        navigator.userAgent.match(/iPhone | iPad | iPod/i) ? template = '<ion-spinner style="margin-left:40%;" icon="ios"></ion-spinner>' : template = '<ion-spinner style="margin-left:40%;" icon="android"></ion-spinner>';

        var popup = $ionicPopup.show({
            template: template,
            title: 'Cargando Zonas',
            scope: $scope,
        });

        $http({
            method: 'POST',
            url: IP + "Ws/get_zonas/1",
            timeout: 5000,
        }).then(function successCallback(response) {
            $scope.zones = response.data.zonas;
            popup.close();
        }, function errorCallback(response) {
            $cordovaToast.show(response.error, "short", "bottom").then(function(success) {}, function(error) {});
        });
    };

    $scope.getColor = function(zone) {
        switch (parseInt(zone.color_tab)) {
            case 0:
            case 6:
                return "item-green";
            case 1:
                return "item-orange";
            case 2:
                return "item-pink";
            case 3:
                return "item-blue";
            case 4:
                return "item-green-black";
            case 5:
                return "item-red";
            default:
                return "";

        }
    };

    $scope.circleColor = function(zone) {
        switch (parseInt(zone.color_tab)) {
            case 0:
            case 6:
                return "img/icons/ic_circle_green.png";
            case 1:
                return "img/icons/ic_circle_orange.png";
            case 2:
                return "img/icons/ic_circle_pink.png";
            case 3:
                return "img/icons/ic_circle_blue.png";
            case 4:
                return "img/icons/ic_circle_green_black.png";
            case 5:
                return "img/icons/ic_circle_red.png";
            default:
                return "";

        }
    }

    if(typeof window.ga !== "undefined") {
                    window.ga.startTrackerWithId("UA-79258652-2");
                    window.ga.trackEvent('ZonasEvent', 'Visit');
                    window.ga.trackView('Zonas');
                    console.log("Google Analytics ZONES Available!!");
                } else {
                    console.log("Google Analytics ZONES Unavailable");
                }

    angular.element(document).ready(function() {
        $scope.getData();
    });

});
gobus.controller('RankingController', function($rootScope, $scope, $state, SessionService, $http, $cordovaToast) {
    $scope.rankings = [];

    $scope.init = function() {
        var formData = new FormData();
        formData.append('id_usuario', $scope.user.id_usuario)
        $http({
            method: 'POST',
            url: IP + 'Ws/get_ranking/1',
            data: formData,
            headers: {
                'Content-Type': undefined
            },
        }).then(function successCallback(response) {
            if (response.data.rankingData.length > 0) {
                $scope.rankings = response.data.rankingData;
                $scope.rankingUser = response.data.currentPosition;
            }
        }, function errorCallback(response) {
            $cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
        });

        $rootScope.img_profile = IP + "uploads/img_users/" + $rootScope.userLogged.img_profile;

        $http({
            method: 'GET',
            url: $rootScope.img_profile
        }).then(function successCallback(response) {

            },
            function errorCallback(response) {
                $rootScope.img_profile = $rootScope.userLogged.img_profile;
                $http({
                    method: 'GET',
                    url: $rootScope.img_profile
                }).then(function successCallback(response) {

                    },
                    function errorCallback(response) {

                        $rootScope.img_profile = "img/icons/placeholder_user.png"
                    });
            });
    };

    $scope.getClassByposition = function(row_number) {
        var classSelector = "";
        if (row_number >= 10 && row_number <= 99) {
            classSelector = "decena";
        } else if (row_number >= 100 && row_number <= 999) {
            classSelector = "centena";
        } else if (row_number >= 1000 && row_number <= 9999) {
            classSelector = "millar";
        }
        return classSelector;
    };

    if(typeof window.ga !== "undefined") {
                    window.ga.startTrackerWithId("UA-79258652-2");
                    window.ga.trackEvent('RankingEvent', 'Visit');
                    window.ga.trackView('Ranking');
                    console.log("Google Analytics RANKING Available!!");
                } else {
                    console.log("Google Analytics RANKING Unavailable");
                }


    $scope.init();
});
gobus.controller('HomeController', function($scope, $rootScope, $ionicSideMenuDelegate, $filter, $cordovaDialogs, $cordovaToast, $ionicModal, $timeout, $state, $ionicPlatform, SessionService, $http, $ionicPopup, $log, $fileLogger, $cordovaVibration) {
    $(".content-page").css('height', $(window).height());
    var TIMEOUT = 5000;
    var connection;
    var navigation = null;
    var searchRouteClose = null;
    var ACTION_SEARCH = 0;
    var ACTION_SHARED = 1;
    var APP_STATUS = ACTION_SEARCH;
    var SELECTED_ROUTES = false;
    var ROUTE_ID = 0;
    var STATUS_SHARED = 0;
    var STOP_SHARED_ROUTE = 1;
    var ERROR_CONEXION = 2;
    var OUTSIDE_RANGE = 3;
    var CLOSE_ABNORMAL = 4;
    var TITLE_ROUTE;
    $scope.sharedLatitude;
    $scope.sharedLongitude;
    $scope.routes = [];
    $rootScope.map = {};
    $scope.route_map = {};
    $scope.data = {
        range: 100
    };
    $scope.routesClose = [];
    $scope.watcherInit = null;
    $scope.markerUser = null;

    $scope.updatePosition = {
        currentLat: 0,
        currentLng: 0,
        beforeLat: 0,
        beforeLng: 0,
    };
    var watcherPositionInit = {};

    $scope.titleHome = "Inicio";


    $rootScope.locationAddress = {};
    $scope.userPosition = {};
    $rootScope.positionOnMap = {};

    $ionicModal.fromTemplateUrl('routesClose.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.routesSearchModal = modal;
    });

    $scope.viewRoutes = function(zone) {
        $state.go('app.routes', {
            zone: JSON.stringify(zone)
        });
    };

    $scope.changeState = function(state) {
        $state.go(state);
    };


    $scope.init = function() {


        var currentPosition;




        if ($rootScope.map != undefined) {
            $rootScope.map = new L.map('map');
        }

        var ggl = new L.Google('ROADMAP');
        $rootScope.map.addLayer(ggl);
        currentPosition = L.latLng(20.96707, -89.623729);
        $scope.positionSearchClose = {
            lat: 20.96707,
            lng: -89.623729
        };
        $rootScope.map.setView(currentPosition, 12);



        $scope.iconUserRed = L.icon({
            iconUrl: 'img/icons/shared-routes/usuario_rojo.png',
            iconRetinaUrl: 'img/icons/shared-routes/usuario_rojo.png',
            iconSize: [45, 30],
            iconAnchor: [24,20]
        });

        $scope.iconUserGreen = L.icon({
            iconUrl: 'img/icons/shared-routes/usuario_verde.png',
            iconRetinaUrl: 'img/icons/shared-routes/usuario_verde.png',
            iconSize: [45, 30],
            iconAnchor: [24,20]
        });

        $scope.gobus = L.icon({
            iconUrl: 'img/icons/markerGobus.png',
            iconSize: [40, 40],
            iconAnchor: [20, 40],
            //popupAnchor: [-3, -76]
        })

        $scope.hcuantico = L.icon({
            iconUrl: 'img/icons/markerHcuantico2.png',
            iconSize: [50, 50],
            iconAnchor: [25, 50],
            //popupAnchor: [-3, -76]
        })

        $scope.saboreate = L.icon({
            iconUrl: 'img/icons/markerSaboreate.png',
            iconSize: [50, 50],
            iconAnchor: [25, 50],
            //popupAnchor: [-3, -76]
        })

        $scope.markerUser = L.marker(currentPosition, {
            icon: $scope.iconUserRed
        });

        var customPopup = "<center>Oficinas Gobus</center><br/>";
        var customPopupHC = "<center>H Cuántico</center><br/>Venta de electrónicos";
        var customPopupSC = "<center>Saboreaté y Café</center><br/>Saboréaté la vida";

        // specify popup options 
        var customOptions = {
            'maxWidth': '500',
            'className': 'custom'
        };

        $scope.publicidad = L.marker([20.9985318, -89.5649018], { icon: $scope.gobus }).bindPopup(customPopup, customOptions).openPopup();
        $scope.Mhcuantico = L.marker([20.9680497, -89.6192701], { icon: $scope.hcuantico }).bindPopup(customPopupHC, customOptions).openPopup();
        $scope.Msaboreate = L.marker([20.970816, -89.621486], { icon: $scope.saboreate }).bindPopup(customPopupSC, customOptions).openPopup();

        $scope.userPosition = L.userMarker(currentPosition, {
            pulsing: true,
            accuracy: $scope.data.range,
            smallIcon: true
        });



        var switchPublicidad = L.easyButton({
            position: 'bottomleft',
            leafletClasses: false,
            states: [{
                stateName: 'stop-bus-off',
                icon: '<img style="margin-bottom: 10px;" src="img/icons/megafonoGris.png"/>',
                title: 'stop bus off',
                onClick: function(btn, map) {
                    $scope.publicidad.addTo($rootScope.map);
                    $scope.Mhcuantico.addTo($rootScope.map);
                    $scope.Msaboreate.addTo($rootScope.map);
                    btn.state('stop-bus-on');
                }
            }, {
                stateName: 'stop-bus-on',
                icon: '<img style="margin-bottom: 10px;" src="img/icons/megafonoRojo.png"/>',
                title: 'stop bus on',
                onClick: function(btn, map) {
                    $rootScope.map.removeLayer($scope.publicidad);
                    $rootScope.map.removeLayer($scope.Mhcuantico);
                    $rootScope.map.removeLayer($scope.Msaboreate);
                    btn.state('stop-bus-off');
                }
            }]
        });
        switchPublicidad.addTo($rootScope.map);

        //icono usuario
        $scope.markerUser.addTo($rootScope.map);

        //icono busqueda
        //$scope.userPosition.addTo($rootScope.map);

        setTimeout(function() {
            navigator.geolocation.getCurrentPosition(function(pos) {
                var lat = pos.coords.latitude
                var lng = pos.coords.longitude
                var position = L.latLng(lat, lng);
                $rootScope.map.setView(position, 14);
                $scope.markerUser.setLatLng(position);
                $scope.userPosition.setLatLng(position);
                $scope.positionSearchClose = {
                    lat: lat,
                    lng: lng
                };
            }, function(error) {}, {
                enableHighAccuracy: true,
            });
        }, 3000);

        $rootScope.map.on('click', function(e) {
            if (APP_STATUS == ACTION_SEARCH) {
                positionClick = e.latlng;
                $scope.positionSearchClose = positionClick;
                $scope.userPosition.setLatLng(positionClick);
                $rootScope.positionOnMap =  $scope.positionSearchClose;
                console.log($rootScope.positionOnMap.lat);
                  console.log($rootScope.positionOnMap.lng);
                $scope.popupSearchRoutesClosed = $ionicPopup.show({
                    template: $scope.getTemplate(),
                    title: 'Buscando rutas',
                    scope: $scope,
                });
                $scope.getRoutesClose(positionClick, ACTION_SEARCH);
            }
        });
        $scope.addButtons();
       $rootScope.hide();
    };
   //CAMBIO PARA BITBUCKET
 $scope.initWatcherPosition = function() {
        cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {    
            console.log(enabled);    
            
            if(!enabled){
                alert("GPS esta " + (enabled ? "habilitado"  : "deshabilitado" + " para poder ofrecer una mejor experiencia de uso, solicitamos activarlo"));
                cordova.plugins.diagnostic.switchToLocationSettings();
            }else{ 
                watcherPositionInit = navigator.geolocation.watchPosition(function(position) {
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;

                $scope.sharedLatitude = lat;
                $scope.sharedLongitude = lng;
                console.log($scope.sharedLatitude,$scope.sharedLongitude);
                
                currentPosition = L.latLng(lat, lng);
                $scope.markerUser.setLatLng(currentPosition);

            },
            function(error) {}, {
                enableHighAccuracy: true,
                timeout: 15000,
                maximumAge: 10000,
            });
             } //AQUI
            }, function(error) {
                alert("The following error occurred: " + error);
            });
        
    }    //FIN CAMBIO PARA BITBUCKET

    $scope.stopWatcherPosition = function() {
        navigator.geolocation.clearWatch(watcherPositionInit);
    }

    $scope.addButtons = function() {
        $scope.searchRouteClose = L.easyButton({
            position: 'topright',
            id: "btn-search-routes-close",
            leafletClasses: false,
            states: [{
                stateName: 'search-data',
                onClick: function(button, map) {
                    $scope.popupSearchRoutesClosed = $ionicPopup.show({
                        template: $scope.getTemplate(),
                        title: 'Buscando rutas',
                        scope: $scope,
                    });
                    $scope.getRoutesClose($scope.positionSearchClose, ACTION_SEARCH);
                },
                title: 'show me the middle',
                icon: 'icon-point-route'
            }]
        }).addTo($rootScope.map);


        $scope.locateButton = L.easyButton('ion ion-android-locate', function(btn, map) {

            cordova.plugins.diagnostic.isLocationAvailable(function(available) {
                if (available) {
                    navigator.geolocation.getCurrentPosition(function(pos) {
                        var lat = pos.coords.latitude

                        var lng = pos.coords.longitude
                        position = L.latLng(lat, lng);
                        $rootScope.map.setView(position, 15);
                        $scope.markerUser.setLatLng(position);
                        //$rootScope.map.flyTo(position);
                    }, function(error) {}, {
                        enableHighAccuracy: true,
                    });
                } else {

                    var confirmPopup = $ionicPopup.confirm({
                        title: 'GPS Deshabilitado',
                        template: 'Para mayor rendimiento habilite su GPS',
                        buttons: [{
                            text: 'Cancelar',
                            type: 'button-assertive'
                        }, {
                            text: 'Habilitar',
                            type: 'button-positive',
                            onTap: function(e) {
                                cordova.plugins.diagnostic.switchToLocationSettings();
                            }
                        }]
                    });
                }
            }, function(error) {

            });
        }).addTo($rootScope.map);



        navigation = L.easyButton({
            id: 'btn-shared-routes',
            position: 'bottomright',
            leafletClasses: true,
            states: [{
                stateName: 'shared',
                onClick: function(button, map) {

                    $scope.popupSearchRoutesClosed = $ionicPopup.show({
                        template: $scope.getTemplate(),
                        title: 'Buscando rutas',
                        scope: $scope,
                    });

                   /* cordova.plugins.diagnostic.isLocationAvailable(function(available) {
                        if (available) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                $scope.updatePosition.beforeLat = position.coords.latitude;
                                $scope.updatePosition.beforeLng = position.coords.longitude;
                                var pos = {
                                    "lat": $scope.sharedLatitude,
                                    "lng": $scope.sharedLongitude
                                };
                                $scope.getRoutesClose(pos, ACTION_SHARED);
                            }, function(error) {
                                $scope.popupSearchRoutesClosed.close()

                            });
                        } else {
                            cordova.plugins.diagnostic.switchToLocationSettings();
                             $scope.popupSearchRoutesClosed.close()
                        }
                    }, function(error) {
                        $scope.popupSearchRoutesClosed.close();

                    });*/

                    var pos = {
                                    "lat": $scope.sharedLatitude,
                                    "lng": $scope.sharedLongitude
                                };
                                $scope.getRoutesClose(pos, ACTION_SHARED);


                },
                title: 'Compartir ubicación',
                icon: 'icon-not-shared-routes'
            }, {
                stateName: 'not-shared',
                icon: 'icon-shared-routes',
                onClick: function(button, map) {
                    STATUS_SHARED = STOP_SHARED_ROUTE;
                    $scope.stopServiceSharedRoutes();
                }
            }]
        });

        //searchRouteClose.addTo($rootScope.map);
        navigation.addTo($rootScope.map);


    }


    $scope.rangeSearch = function() {
        $scope.markerUser.setAccuracy($scope.data.range);
    };

    $scope.sharedRoutes = function(route) {
        ROUTE_ID = route.id_ruta;
        $scope.routesSearchModal.hide();
        $scope.titleHome = "Compartiendo " + route.nom_ruta;
        TITLE_ROUTE = route.nom_ruta;
        $scope.drawRoute(route.id_ruta);
    };

    $scope.runServiceShared = function() {
        connection = new WebSocket(SOCKET_SHARED);
        connection.onopen = function(e) {

            $scope.popupSearchRoutesClosed.close();
            navigation.state('not-shared');
            $scope.markerUser.setIcon($scope.iconUserGreen);
            $scope.map.removeLayer($scope.userPosition);
            APP_STATUS = ACTION_SHARED;
            $scope.stopWatcherPosition();
            $(".ion-android-locate").parents('button').hide();
            $rootScope.map.removeControl($scope.searchRouteClose);
            $ionicSideMenuDelegate.canDragContent(false);
            $rootScope.showMenuButton = false;
            //$rootScope.map.removeControl($scope.zoomControl)

            navigator.geolocation.getCurrentPosition(function(pos) {
                var lat = pos.coords.latitude;
                var lng = pos.coords.longitude;

                var currentPosition = new Object();
                currentPosition.lat = lat;
                currentPosition.lng = lng;
                currentPosition.id_ruta = ROUTE_ID;
                currentPosition.id_user = $rootScope.userLogged.id_usuario;
                currentPosition.date = $filter('date')(new Date().toISOString(), 'yyyy-MM-dd hh:mm:ss');
                $scope.sendPosition(currentPosition);

                $scope.initWatcherServiceShared();
                $scope.initBackgroundServiceLocation();

            }, function(error) {}, {
                enableHighAccuracy: true,
                timeout: 5000
            });
        };

        connection.onmessage = function(e) {
            var v = JSON.parse(e.data);
            if (v.action == "outside_route") {
                STATUS_SHARED = OUTSIDE_RANGE;
                connection.close();
            }
        };

        connection.onerror = function(evt) {
            $scope.stopServiceSharedRoutes();
            $scope.popupSearchRoutesClosed.close();
        }

        connection.onclose = function(evt) {
            if (evt.code != 1000) {
                $cordovaToast.show("Ocurrió un error al intentar conectarse al servicio, intenta más tarde y contactáctanos si el error persiste.", "short", "bottom").then(function(success) {}, function(error) {});
                $scope.popupSearchRoutesClosed.close();
            } else {

                if (evt.code == 1006) {
                    $cordovaVibration.vibrate(700);
                    $cordovaDialogs.alert('Su dispositivo dejó de enviar su ubicación para compartir y ha sido desconectado.', 'Info', 'Aceptar')
                        .then(function() {
                            // callback success
                        });
                } else {
                    switch (STATUS_SHARED) {
                        case OUTSIDE_RANGE:
                            $cordovaVibration.vibrate(1000);
                            $cordovaDialogs.alert('Hemos detectado que se encuentra fuera de la ruta ' + TITLE_ROUTE + '. Gracias por compartir tu ruta.', 'Fuera de la ruta', 'ok')
                                .then(function() {
                                    // callback success
                                    
                                });
                            break;
                        case ERROR_CONEXION:
                            $cordovaVibration.vibrate(700);
                            $cordovaToast.show("Ocurrió un error al intentar conectarse al servicio, intenta más tarde y contactáctanos si el error persiste.", "short", "bottom").then(function(success) {}, function(error) {});
                            break;
                        case STOP_SHARED_ROUTE:
                            
                            $cordovaDialogs.alert('Gracias por compartir tu ruta.', '¡Gracias!', 'ok')
                                .then(function() {
                                    // callback success
                                    
                                });
                            break;
                    }
                }
            }
            $scope.stopServiceSharedRoutes();

        };

    };

    $scope.initBackgroundServiceLocation = function() {
        $scope.bgLocationServices.start();

        $scope.bgLocationServices.registerForLocationUpdates(function(location) {
            var lat = location.latitude;
            var lng = location.longitude;
            var currPosition = L.latLng(lat, lng);
            $scope.markerUser.setLatLng(currPosition);
            $rootScope.map.setView(currPosition);
            var currentPosition = new Object();
            currentPosition.lat = lat;
            currentPosition.lng = lng;
            currentPosition.id_ruta = ROUTE_ID;
            currentPosition.id_user = $rootScope.userLogged.id_usuario;
            currentPosition.date = $filter('date')(new Date().toISOString(), 'yyyy-MM-dd hh:mm:ss');

            $scope.sendPosition(currentPosition);
        }, function(err) {

            //$cordovaToast.show(err.message, "short", "bottom").then(function(success) {}, function(error) {});
        });

    }

    $scope.sendPosition = function(position) {
        if (connection.readyState === WebSocket.OPEN) {
            connection.send(JSON.stringify(position));
        } else {
            STATUS_SHARED = ERROR_CONEXION;
            $scope.stopServiceSharedRoutes();
        }
    }

    var currentDate;
    $scope.initWatcherServiceShared = function() {
        currentDate = Math.floor(Date.now() / 1000);
        $scope.watcherInit = navigator.geolocation.watchPosition(function(pos) {
                $scope.updatePosition.currentLat = pos.coords.latitude;
                $scope.updatePosition.currentLng = pos.coords.longitude;
                var latBool = $scope.updatePosition.currentLat != $scope.updatePosition.beforeLat;
                var lngBool = $scope.updatePosition.currentLng != $scope.updatePosition.beforeLng;
                var newDate = Math.floor(Date.now() / 1000);
                var boolDate = newDate - currentDate;

                if (boolDate >= 15) {
                    currentDate = newDate;
                    var lat = pos.coords.latitude;
                    var lng = pos.coords.longitude;
                    position = L.latLng(lat, lng);
                    $scope.markerUser.setLatLng(position);
                    $rootScope.map.setView(position);
                    var currentPosition = new Object();
                    currentPosition.lat = lat;
                    currentPosition.lng = lng;
                    currentPosition.id_ruta = ROUTE_ID;
                    currentPosition.id_user = $rootScope.userLogged.id_usuario;
                    currentPosition.date = $filter('date')(new Date().toISOString(), 'yyyy-MM-dd hh:mm:ss');
                    $scope.sendPosition(currentPosition);


                    $scope.updatePosition.beforeLat = $scope.updatePosition.currentLat;
                    $scope.updatePosition.beforeLng = $scope.updatePosition.currentLng;
                }

            },
            function(error) {}, {
                enableHighAccuracy: true,
                timeout: 15000,
                maximumAge: 10000,
            });
    };


    $scope.stopServiceSharedRoutes = function() {
        $scope.titleHome = "Inicio";
        navigation.state('shared');

        $ionicSideMenuDelegate.canDragContent(true);
        $rootScope.showMenuButton = true;

        //$rootScope.map.addControl($scope.zoomControl);
        //$rootScope.map.addControl($scope.locateButton);
        $(".ion-android-locate").parents('button').show();
        $rootScope.map.addControl($scope.searchRouteClose);
        navigator.geolocation.clearWatch($scope.watcherInit);
        if (connection.readyState === WebSocket.OPEN) {
            connection.close();
        }
        $scope.bgLocationServices.stop();

        $scope.updatePosition = {
            currentLat: 0,
            currentLng: 0,
            beforeLat: 0,
            beforeLng: 0
        }
        $scope.watcherInit = null;
        if ($scope.route_map != !undefined) {
            $rootScope.map.removeLayer($scope.route_map.objectRuta);
        }
        APP_STATUS = ACTION_SEARCH;
        $scope.markerUser.setIcon($scope.iconUserRed);
        $scope.userPosition.addTo($rootScope.map);
        $scope.initWatcherPosition();
    };

    $scope.drawRoute = function(route_id) {
        $scope.popupSearchRoutesClosed = $ionicPopup.show({
            template: $scope.getTemplate(),
            title: 'Buscando rutas',
            scope: $scope,
        });


        $.ajax({
            type: 'POST',
            url: IP + 'Ws/get_geo_json_by_id',
            data: {
                id: route_id
            },
            timeout: 5000,
            success: function(response) {
                var geoJsonObjectRuta = $scope.getGeoJsonObject(response, 'RUTA');
                var geoJsonObjectParaderos = $scope.getGeoJsonObject(response, 'PARADEROS');
                $scope.route_map = {
                    id: route_id,
                    objectRuta: geoJsonObjectRuta,
                    objectParaderos: geoJsonObjectParaderos
                };
                geoJsonObjectRuta.addTo($rootScope.map);
                $rootScope.map.fitBounds($scope.route_map.objectRuta.getBounds());
                $scope.runServiceShared(route_id);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $scope.popupSearchRoutesClosed.close();
                $scope.showErrorMessages(jqXHR.status);
            },
            dataType: 'JSON'
        });
    };

    $scope.getGeoJsonObject = function(json, propertyType) {
        return L.geoJson(json, {
            pointToLayer: function(feature, latlng) {
                var marker = new L.CircleMarker(latlng, {
                    radius: 7,
                    fillColor: "#ffffff",
                    fillOpacity: 1,
                    color: "#04B486",
                    weight: 5,
                    opacity: 1
                });
                marker.bindPopup("Paradero");
                return marker;
            },
            onEachFeature: function(feature, layer) {
                if (feature.geometry.type == "LineString") {
                    layer.setText('\u279c            ', {
                        repeat: true,
                        below: true,
                        center: true,
                        attributes: {
                            fill: '#0a07ff',
                            'font-weight': 'bold',
                            'font-size': '20'
                        }
                    });
                } else if (feature.geometry.type == "MultiPoint") {}
            },
            filter: function(feature, layer) {
                return feature.properties.type == propertyType;
            },
            style: function(feature) {
                if (feature.geometry.type == "LineString") {
                    return {
                        fillColor: '#d26d71',
                        fillOpacity: 1,
                        color: '#d26d71',
                        dashArray: '0',
                        weight: 4,
                        opacity: 1
                    };
                } else if (feature.geometry.type == "MultiPoint") {
                    return {
                        radius: 7,
                        fillColor: "#ffffff",
                        fillOpacity: 1,
                        color: "#d26d71",
                        weight: 5,
                        opacity: 1
                    };
                }
            }
        });
    };

    $scope.getRoutesClose = function(latLng, action) {
        $scope.routesClose = [];
        $.ajax({
            type: 'POST',
            url: IP + 'Ws/get_routes_close',
            data: {
                lat: latLng.lat,
                lng: latLng.lng,
                distance: $scope.data.range
            },
            timeout: 5000,
            success: function(response) {
                if (response.list_rutas.length > 0) {

                    if (action == ACTION_SEARCH) {
                        $.ajax({
                            type: 'GET',
                            url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLng.lat + ',' + latLng.lng,
                            timeout: 5000,
                            success: function(responseGoogleMaps) {
                                if (responseGoogleMaps.status == "OK") {
                                    var address = responseGoogleMaps.results[0];
                                    var getAddress = $scope.getAddress(address);
                                    if (getAddress != null) {
                                        $rootScope.locationAddress = getAddress;
                                    } else {
                                        $rootScope.locationAddress.colonia = "Not found";
                                        $rootScope.locationAddress.address = "Not found";
                                    }
                                }
                                $scope.popupSearchRoutesClosed.close();
                                $state.go('app.results', {
                                    results: JSON.stringify(response.list_rutas)
                                });
                                //$scope.runServiceShared(route_id);
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                $scope.popupSearchRoutesClosed.close();
                                $scope.showErrorMessages(jqXHR.status);
                            },
                            dataType: 'JSON'
                        });

                    } else {
                        $scope.routesClose = response.list_rutas;
                        $scope.routesSearchModal.show();
                        $scope.popupSearchRoutesClosed.close();
                    }
                } else {
                    $scope.popupSearchRoutesClosed.close();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Sin resultados',
                        template: 'No se encontraron resultados'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $scope.popupSearchRoutesClosed.close();
                $scope.showErrorMessages(jqXHR.status);
            },
            dataType: 'JSON'
        });
    };


    $scope.getAddress = function(addressMaps) {
        var address = null;
        angular.forEach(addressMaps.address_components, function(value) {
            var types = value.types;
            for (var i = 0; i < types.length; i++) {
                if (types[i] == "sublocality") {
                    address = {
                        colonia: value.short_name,
                        address: addressMaps.formatted_address
                    };
                }
            }
        });
        return address;
    }


    $scope.getTemplate = function() {
        var template = "";
        navigator.userAgent.match(/iPhone | iPad | iPod/i) ? template = '<ion-spinner style="margin-left:40%;" icon="ios"></ion-spinner>' : template = '<ion-spinner style="margin-left:40%;" icon="android"></ion-spinner>';
        return template;
    };

    $scope.closeModalRoutesShared = function($event) {
        $scope.markerUser.setIcon($scope.iconUserRed);
        $scope.routesSearchModal.hide();
        navigation.state('shared');
        APP_STATUS = ACTION_SEARCH;
    }

    $scope.showErrorMessages = function(response) {
        var message = ""
        switch (response.status) {
            case 500:
                message = "Ocurrió un error al intentar procesar su solicitud, intentelo de nuevo más tarde o contacte al administrador del sistema";
                break;
            case 408:
            case 504:
                message = "El servidor tardó demasiado en responder";
                break;
            case 503:
                message = "El servicio no se encuentra disponible por el momento, intentelo de nuevo más tarde o conctacte al administrador del sistema";
                break;
            case 204:
                message = "No se encontró la respuesta solicitada, intentelo nuevamente";
            default:
                message = "Ocurió un error, consulte al administrador";

        }

        $cordovaToast.show(message, "long", "bottom").then(function(success) {}, function(error) {});
    }

    /**Aqui se inicia todo el sistema */


    angular.element(document).ready(function() {
        $timeout(function() {
            $scope.bgLocationServices = window.plugins.backgroundLocationServices;
            $scope.bgLocationServices.configure({
                desiredAccuracy: 3,
                distanceFilter: 10,
                debug: false,
                interval: 8000,
                fastestInterval: 8000,
                useActivityDetection: false,
                notificationTitle: 'Compartir Ruta',
                notificationText: 'Compartiendo tu ruta'
            });
            $scope.initWatcherPosition();
        }, 5000);

        $scope.init();
    });


});
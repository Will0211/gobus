gobus.controller('RoutesController', function($scope, $state, $stateParams, $http, $ionicPopup, SessionService, $cordovaToast) {
    $(".content-page").css('height', $(window).height());
    $scope.routes = [];

    $scope.zone = JSON.parse($stateParams.zone);

    $scope.routeDetail = function(route) {
        $state.go('app.route', {
            route: JSON.stringify(route)
        });
    };

    $scope.getData = function() {

        var template = "";
        navigator.userAgent.match(/iPhone | iPad | iPod/i) ? template = '<ion-spinner style="margin-left:40%;" icon="ios"></ion-spinner>' : template = '<ion-spinner style="margin-left:40%;" icon="android"></ion-spinner>';

        var popup = $ionicPopup.show({
            template: template,
            title: 'Cargando rutas',
            scope: $scope,
        });

        $.ajax({
            type: 'POST',
            url: IP + "Ws/list_rutas/1/" + $scope.zone.id_zona,
            timeout: 5000,
            success: function(response) {
                $scope.routes = response.list_rutas;
                popup.close();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                popup.close();
                $scope.showErrorMessages(jqXHR.status);
            },
            dataType: 'JSON'
        });


    };

    $scope.showErrorMessages = function(response) {
        var message = ""
        switch (response.status) {
            case 500:
                message = "Ocurrio un erro al intentar procesar su solicitud, intentelo de nuevo mas tarde o contacte al administrador del sistema";
                break;
            case 408:
            case 504:
                message = "El servidor tardo demasiado en responder";
                break;
            case 503:
                message = "El servicio no se encuentra disponible por el momento, intentelo de nuevo mas tarde o conctacte al administrador del sistema";
                break;
            case 204:
                message = "No se encontro la respuesta solicitada, intentelo nuevamente";
            default:
                message = "Ocurio un error consulte al administrador";

        }

        $cordovaToast.show(message, "long", "bottom").then(function(success) {}, function(error) {});
    }

    angular.element(document).ready(function() {
        $scope.getData();
    });
});
gobus.controller('ResetPassController', function($scope, $rootScope, $state, $ionicPopup, $cordovaToast, $http, $cordovaDialogs, RecoveryService) {
   
    $scope.user = {};
   $scope.returnLogin = function() {
        $state.go('login');
    };

    $scope.resetPass = function()
    {
        console.log('resett');
       
            var user =  $scope.user;
            console.log(user.userName);
            RecoveryService.recuperarPassword(user.userName,function(response){
                if(response.result)
                {
                    var confirmPopup = $ionicPopup.alert({
                    title: 'Listo',
                    template: 'Se te enviará un correo de recuperación de contraseña. No olvides revisar tu Correo no Deseado.'
                    });
                    confirmPopup.then(function(res) {
                        if (res) 
                        {
                            $state.go('login');
                        } 
                        else 
                        {

                        }
                    }); 

                }
                else
                {
                    var confirmPopup = $ionicPopup.alert({
                    title: 'Error',
                    template: response.mensaje
                    });
                    confirmPopup.then(function(res) {
                        if (res) 
                        {
                            
                        } 
                        else 
                        {

                        }
                    }); 
                }
            });
            
           

        
    };
    
});
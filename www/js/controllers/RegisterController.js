var VALIDATION_FIELDS = "Todos los campos son obligatorios";
var MATCH_PASSWORD = "Las contraseñas no coinciden";
var TERMS_CONDITION = "Debe aceptar los términos y condiciones";

gobus.controller('RegisterController', function($scope, $rootScope, $state, $ionicPopup, $cordovaToast, $http, SessionService) {
    $(".content-page").css('height', $(window).height());
    $scope.user = {};
    $scope.openTermsConditions = function() {

        var confirmPopup = $ionicPopup.confirm({
            title: 'Terminos y condiciones',
            templateUrl: 'terms-condition.html'
        });
        confirmPopup.then(function(response) {
            if (response) {
                $scope.user.terms = true;
            } else {
                $scope.user.terms = false;
            }
        });
    };

    $scope.returnLogin = function() {
        $state.go('login');
    };

    $scope.registerUser = function() {
        if ($scope.validateUser($scope.user) == false) {
            $scope.showToast(VALIDATION_FIELDS, "short", "bottom");
        } else {
            if ($scope.matchPassword($scope.user.password, $scope.user.confirm_password)) {
                if ($scope.user.terms != undefined && $scope.user.terms) {
                    var user = $scope.user;
                    var formUser = new FormData();

                    formUser.append('name', user.name);
                    formUser.append('email', user.email);
                    formUser.append('pass', user.password);
                    formUser.append('status', 1);
                    formUser.append('registration_type', 1);
                    formUser.append('user_type', 0);
                    formUser.append('id_device', 0);

                    $scope.saveUser(formUser);
                } else {
                    $scope.showToast(TERMS_CONDITION, "short", "bottom");
                }
            } else {
                $scope.showToast(MATCH_PASSWORD, "short", "bottom");
            }
        }
    };

    $scope.validateUser = function(user) {
        if ($scope.trim(user.name) == false || $scope.trim(user.email) == false || $scope.trim(user.password) == false || $scope.trim(user.confirm_password) == false) {
            return false;
        } else {
            return true;
        }
    };

    $scope.trim = function(value) {
        if (value == undefined || value.replace(/ /g, '') == "") {
            return false;
        } else {
            return true;
        }

    }

    $scope.showToast = function(message, duration, location) {
        $cordovaToast.show(message, duration, location).then(function(success) {}, function(error) {});
    };

    $scope.matchPassword = function(password, confirm_password) {
        if (password.replace(/ /g, '') != confirm_password.replace(/ /g, '')) {
            return false
        } else {
            return true;
        }
    };

    $scope.saveUser = function(formData) {
        $http({
            method: 'POST',
            url: URL_REST + "verificarnuevousuario",
            data: formData,
            processData: false,
            headers: {
                'Content-Type': undefined
            }
        }).then(function successCallback(response) {
            console.log(response);
            console.log(response.result);
            if (response.result) {
              
                $state.go('login');
            } else {
                $cordovaToast.show(response.data.response, "short", "bottom").then(function(success) {}, function(error) {});
            }
        }, function errorCallback(response) {
            $cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
        });

    };

    if(typeof window.ga !== "undefined") {
                    window.ga.startTrackerWithId("UA-79258652-2");
                    window.ga.trackEvent('RegisterEvent', 'Visit');
                    window.ga.trackView('Register');
                    console.log("Google Analytics REGISTER Available!!");
                } else {
                    console.log("Google Analytics REGISTER Unavailable");
                }

});
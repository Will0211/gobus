gobus.controller('AppController', function($scope, $ionicSideMenuDelegate, $rootScope, $ionicModal, $timeout, $state, SessionService, $ionicPopup, $ionicHistory, $http, $cordovaToast, $q) {
    $scope.user = $rootScope.userLogged;
    $scope.search = {};
    $scope.routes = [];
    $scope.promises = [];
    $rootScope.showMenuButton = true;
    $scope.destinies = [];

    $scope.init = function() {
        $rootScope.image_profile = IP + "uploads/img_users/" + $scope.user.img_profile;

        $http({
            method: 'GET',
            url: $rootScope.image_profile,
            timeout: "5000",
        }).then(function successCallback(response) {},
            function errorCallback(response) {
                $rootScope.image_profile = $scope.user.img_profile;
                $http({
                    method: 'GET',
                    url: $rootScope.image_profile,
                    timeout: "5000",
                }).then(function successCallback(response) {},
                    function errorCallback(response) {
                        $rootScope.image_profile = "img/icons/placeholder_user.png"
                    });
            });

    };

    $scope.$watch(function() {
            return $ionicSideMenuDelegate.isOpenRight();
        },
        function(open) {
            if (!open) {
                if ($rootScope.map != undefined) {
                    $rootScope.map._onResize();
                }
            }
        });

    $scope.$watch(function() {
            return $ionicSideMenuDelegate.getOpenRatio();
        },
        function(ratio) {
            if ($rootScope.map != undefined) {
                $rootScope.map._onResize();
            }
        });




    $scope.searchRedirect = function() {
        $state.go('app.search');
    };

    $scope.changeState = function(state) {
        $state.go(state);
    }

    $scope.backStory = function() {

        switch ($ionicHistory.backView().stateName) {
            case "app.home":
                if ($rootScope.map != undefined) {
                    $rootScope.map._onResize();
                }
                $ionicHistory.goBack();
                break;
            case "app.route":
                if ($rootScope.mapRoute != undefined) {
                    $rootScope.mapRoute._onResize();
                }
                $ionicHistory.goBack();
                break;
            default:
                $ionicHistory.goBack();
                break;
        }
    }

    $scope.closeSession = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Cerrar sesión',
            template: '¿Estás seguro que deseas cerrar tu sesión?'
        });
        confirmPopup.then(function(res) {
            if (res) {
                SessionService.destroyAll();
                $state.go('login', {
                    flag: 1
                }, {
                    reload: true
                });
            } else {

            }
        });
    };

    $scope.searchRoutes = function(search) {
        //cambio BITBUCKET
         $rootScope.positionOnMap = null;
         //FINCAMBIO
        $http({
            method: 'GET',
            url: IP + "/Ws/list_routes_like_search/" + search + "/1",
            timeout: "5000",
            headers: {
                'Content-Type': undefined
            },
        }).then(function successCallback(response) {
            if (response.data.length > 0) {
                $scope.routes = response.data;
            } else {
                $scope.routes = [];
                $cordovaToast.show("No se encontraron resultados", "short", "bottom").then(function(success) {}, function(error) {});
            }
        }, function errorCallback(response) {
            //$cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
        });
    };

    $scope.getSearch = function(val) {
        $scope.destinies = [];
        if ($scope.promises.length > 0) {
            angular.forEach($scope.promises, function(value, key) {
                value.resolve();
            });
            $scope.promises = [];
        }

        var ajaxCall = $q.defer();
        $scope.promises.push(ajaxCall);
        if (val != undefined && val.replace(/ /g, "") != "") {
            $http({
                method: 'GET',
                url: IP + "/Ws/list_destinos_like_search/" + val + "/1",
                timeout: ajaxCall.promise,
                headers: {
                    'Content-Type': undefined
                },
            }).then(function successCallback(response) {
                if (response.data.length > 0) {
                    var d = []
                    angular.forEach(response.data, function(value) {
                        $scope.destinies.push(value.name)
                    });
                } else {
                    $scope.destinies = [];
                    $cordovaToast.show("No se encontraron resultados", "short", "bottom").then(function(success) {}, function(error) {});
                }
            }, function errorCallback(response) {
                //$cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
            });
            //console.log(cancel);
            //cancel.resolve();
        } else {
            $scope.destinies = [];
        }
    };


    $scope.getImageStop = function(url) {
        if (url != null) {
            return IP + "uploads/img_ruta/" + url;
        } else {
            return "img/icons/not-found.png"
        }
    };

    $scope.viewRoute = function(result) {
        $state.go('app.route', {
            route: JSON.stringify(result)
        });
    };

    $scope.editProfile = function() {
        $state.go('app.profile');
    };

    $scope.init();
});
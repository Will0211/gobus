gobus.controller('LoginController', function($scope, $rootScope, $state, $cordovaOauth, $http, $cordovaToast, SessionService, $ionicHistory, $stateParams) {
    $(".content-page").css('height', $(window).height());
    $scope.user = {};
    $rootScope.userLogged = {};
    if (SessionService.getData('user') != undefined && SessionService.getData('user').status == "ok") {
        $rootScope.userLogged.id_usuario = SessionService.getData('user').id_usuario;
        $rootScope.userLogged.registration_type = SessionService.getData('user').registration_type;
        $rootScope.userLogged.status = SessionService.getData('user').status;
        $rootScope.userLogged.email = SessionService.getData('user').email;
        $rootScope.userLogged.img_profile = SessionService.getData('user').img_profile;
        $rootScope.userLogged.name = SessionService.getData('user').name;

        $state.go('app.home');
    }


    $scope.reset = function()
    {
        $state.go('resetPassword');
    }
    
    if (parseInt($stateParams.flag) == 1) {
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
    }

    $scope.login = function(user) {

        var formUser = new FormData();
        formUser.append('email', user.email);
        formUser.append('pass', user.password);


        $http({
            method: 'POST',
            url: IP + "Ws/logingobus",
            data: formUser,
            headers: {
                'Content-Type': undefined
            },
        }).then(function successCallback(response) {
            if (response.data.status == "ok") {

                var user = new User(response.data.user.id_usuario, response.data.user.registration_type, response.data.status, response.data.user.email, response.data.user.img_profile, response.data.user.name);
                SessionService.setData('user', user);

                $rootScope.userLogged = user;

                if ($rootScope.map != undefined) {
                    $rootScope.map._onResize();
                }
                if ($rootScope.mapRoute != undefined) {
                    $rootScope.mapRoute._onResize();
                }
                $state.go('app.home');
            } else {
                $cordovaToast.show(response.data.response, "short", "bottom").then(function(success) {}, function(error) {});
            }
        }, function errorCallback(response) {
            $cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
        });


    };

    $scope.register = function() {
        $state.go('register');
    };

    $scope.loginFacebook = function() {
        /**
         * Metodo de la Graph Api para seleccionar tu imagen de perfil de facebookConnectPlugin
         * userID/picture?type=large
         * Regresa en este formato
         * {
         * "data": {
         * "is_silhouette": false,
         * "url": "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/15726804_1074926825962692_348566963073642174_n.jpg?oh=3511bebf95e4e5e68deee35bf2cf1e58&oe=58D7A3DC"
         * }
         * }
         */

        facebookConnectPlugin.getLoginStatus(function(response) {
            if (response.status === "connected") {
                var URL_REQUEST = response.authResponse.userID + "/?fields=id,name,email,link,gender,age_range,picture.width(300)";
                facebookConnectPlugin.api(URL_REQUEST, ["email", "public_profile"],
                    function onSuccess(responseData) {

                        var formUser = new FormData();
                        formUser.append('name', responseData.name);
                        formUser.append('email', responseData.email);
                        formUser.append('pass', responseData.id);
                        formUser.append('status', 1);
                        formUser.append('registration_type', 0);
                        formUser.append('user_type', 0);
                        formUser.append('id_device', 0);
                        formUser.append('img_profile', responseData.picture.data.url);
                        formUser.append('link', responseData.link);
                        formUser.append('id_facebook', responseData.id);
                        formUser.append('age_birthday', responseData.age_range.min);
                        formUser.append('gender', responseData.gender);

                        $scope.saveUser(formUser);
                    },
                    function onError(errorResponse) {

                    }
                );
            } else {
                facebookConnectPlugin.login(["email", "public_profile"], function(response) {
                    var URL_REQUEST = response.authResponse.userID + "/?fields=id,name,email,link,gender,age_range,picture.width(300)";
                    facebookConnectPlugin.api(URL_REQUEST, ["email", "public_profile"],
                        function onSuccess(responseData) {
                            var formUser = new FormData();
                            formUser.append('name', responseData.name);
                            formUser.append('email', responseData.email);
                            formUser.append('pass', responseData.id);
                            formUser.append('status', 1);
                            formUser.append('registration_type', 0);
                            formUser.append('user_type', 0);
                            formUser.append('id_device', 0);
                            formUser.append('img_profile', responseData.picture.data.url);
                            formUser.append('link', responseData.link);
                            formUser.append('id_facebook', responseData.id);
                            formUser.append('age_birthday', responseData.age_range.min);
                            formUser.append('gender', responseData.gender);

                            $scope.saveUser(formUser);
                        },
                        function onError(errorResponse) {

                        }
                    );
                }, function(error) {

                });
            }

        }, function(error) {});

    };

    $scope.saveUser = function(formData) {
        $http({
            method: 'POST',
            url: IP + "Ws/user_register",
            data: formData,
            processData: false,
            headers: {
                'Content-Type': undefined
            }
        }).then(function successCallback(response) {
            if (response.data.status == "ok") {

                var user = new User(response.data.user.id_usuario, response.data.user.registration_type, response.data.status, response.data.user.email, response.data.user.img_profile, response.data.user.name);
                SessionService.setData('user', user);

                $rootScope.userLogged.id_usuario = response.data.user.id_usuario;
                $rootScope.userLogged.registration_type = response.data.user.registration_type;
                $rootScope.userLogged.status = response.data.user.status;
                $rootScope.userLogged.email = response.data.user.email;
                $rootScope.userLogged.img_profile = response.data.user.img_profile;
                $rootScope.userLogged.name = response.data.user.name;
                $rootScope.image_profile = response.data.user.img_profile

                if ($rootScope.map != undefined) {
                    $rootScope.map._onResize();
                }
                if ($rootScope.mapRoute != undefined) {
                    $rootScope.mapRoute._onResize();
                }


                $state.go('app.home');
            } else {
                $cordovaToast.show(response.data.response, "short", "bottom").then(function(success) {}, function(error) {});
            }
        }, function errorCallback(response) {
            $cordovaToast.show(response, "short", "bottom").then(function(success) {}, function(error) {});
        });

    };

    $scope.toggleLeft = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };

    angular.element(document).ready(function() {
        $rootScope.hide();
    });




});
gobus.controller('RouteController', function($scope, $rootScope, $state, $stateParams, $http, $ionicPopup, SessionService, $log, $fileLogger, $cordovaToast) {
    $rootScope.mapRoute = {};
    $scope.route_map = {};
    $scope.currentPosition = 0;
    $scope.route = JSON.parse($stateParams.route);
    $scope.points = [];




    $scope.getDetail = function(route) {
        $rootScope.abConnection.unsubscribe(Object.keys($rootScope.abConnection._subscriptions)[0]);
        $state.go('app.route_detail', {
            route: JSON.stringify(route)
        });
    };

    $scope.init = function() {
        $rootScope.mapRoute = L.map('map-route');
        var ggl = new L.Google('ROADMAP');
        $rootScope.mapRoute.addLayer(ggl);
        //$scope.addButtons();

        var template = "";
        navigator.userAgent.match(/iPhone | iPad | iPod/i) ? template = '<ion-spinner style="margin-left:40%;" icon="ios"></ion-spinner>' : template = '<ion-spinner style="margin-left:40%;" icon="android"></ion-spinner>';
        console.log('mensaje');
        $scope.popup = $ionicPopup.show({
            
            template: template,
            title: 'Cargando ruta',
            scope: $scope,
        });
        

         $scope.setMapRoute();
       
    
        if (!("WebSocket" in window)) {
            var alertPopup = $ionicPopup.alert({
                title: 'No disponible',
                template: 'Esta caracteristica no esta disponible en su dispositivo, por lo que no se puede ver los autobuses en tiempo real',
                scope: $scope
            });
        }


    };

    $scope.setMapRoute = function() {

         var blueDot = L.icon({
            iconUrl: 'bluedot.png',
            iconAnchor: [22,22],
            iconSize: [2,2],
            className: 'leaflet-usermarker-small'
        })

        if($rootScope.positionOnMap == null)
        {

        }
        else
        {
            var mark2 = L.userMarker([$rootScope.positionOnMap.lat, $rootScope.positionOnMap.lng], {
            html: '<i class="pulse2"></i>',
            icon: blueDot
            }).addTo($rootScope.mapRoute);
            mark2.bindPopup("Punto seleccionado");
        }


        $.ajax({
            type: 'POST',
            url: IP + 'Ws/get_geo_json_by_id',
            data: {
                id: $scope.route.id_ruta
            },
            timeout: 5000,
            success: function(response) {
                var geoJsonObjectRuta = $scope.getGeoJsonObject(response, 'RUTA');
                var geoJsonObjectParaderos = $scope.getGeoJsonObject(response, 'PARADEROS');
                $scope.route_map = {
                    id: $scope.route.id_ruta,
                    objectRuta: geoJsonObjectRuta,
                    objectParaderos: geoJsonObjectParaderos
                };
                geoJsonObjectRuta.addTo($rootScope.mapRoute);
                $rootScope.mapRoute.fitBounds($scope.route_map.objectRuta.getBounds());

                $scope.connectionSocket();
                $scope.popup.close();
                $scope.getUbication();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $scope.popup.close();
                $scope.showErrorMessages(jqXHR.status);
            },
            dataType: 'JSON'
        });
        //CAMBIO BITBUCKET
        //$rootScope.mapRoute.setZoom(13);
        //FINCAMBIO
    };

    $scope.setUbication = function(lat, lng) {
        $scope.currentPosition = L.latLng(lat, lng);
       // $rootScope.mapRoute.setView($scope.currentPosition, 13);
        var markperPosition = L.AwesomeMarkers.icon({
            icon: 'ion ion-android-pin',
            markerColor: 'blue'
        });

        var mark = L.marker([lat, lng], {
            icon: markperPosition
        }).addTo($rootScope.mapRoute);
        mark.bindPopup("Tu ubicación");

    };


    $scope.connectionSocket = function() {
        var markperBus = L.icon({
            iconUrl: 'img/icons/bus.png',
            iconSize: [40, 40]
        });
        $rootScope.abConnection = undefined;

        $rootScope.abConnection = new ab.Session(SOCKET_ROUTE,
            function(session) {
                $rootScope.abConnection.subscribe(JSON.stringify({
                    ruta_id: $scope.route.id_ruta
                }), function(data, response) {
                    console.log(response);
                    var markers = JSON.parse(response);
                    if ($scope.points.length > 0) {
                        angular.forEach($scope.points, function(value, key) {
                            $rootScope.mapRoute.removeLayer(value);
                        });
                        $scope.points = [];
                    }
                    if (markers.length > 0) {
                        $scope.points = [];
                        angular.forEach(markers, function(value, key) {
                            var marker = L.marker([value.lat, value.lng], {
                                icon: markperBus
                            });
                            $scope.points.push(marker);
                            marker.addTo($rootScope.mapRoute);
                        });
                    }
                });
            },
            function(error) {}, {
                'skipSubprotocolCheck': true
            }
        );

    };



    $scope.getGeoJsonObject = function(json, propertyType) {
        return L.geoJson(json, {
            pointToLayer: function(feature, latlng) {
                var marker = new L.CircleMarker(latlng, {
                    radius: 7,
                    fillColor: "#ffffff",
                    fillOpacity: 1,
                    color: "#04B486",
                    weight: 5,
                    opacity: 1
                });
                marker.bindPopup("Paradero");
                return marker;
            },
            onEachFeature: function(feature, layer) {
                if (feature.geometry.type == "LineString") {
                    layer.setText('\u279c            ', {
                        repeat: true,
                        below: false,
                        center: true,
                        offset: 7,
                        attributes: {
                            fill: '#0a07ff',
                            'font-weight': 'bold',
                            'font-size': '18'
                        }
                    });
                } else if (feature.geometry.type == "MultiPoint") {

                }
            },
            filter: function(feature, layer) {
                return feature.properties.type == propertyType;
            },
            style: function(feature) {
                if (feature.geometry.type == "LineString") {
                    return {
                        fillColor: '#d26d71',
                        fillOpacity: 1,
                        color: '#d26d71',
                        dashArray: '0',
                        weight: 4,
                        opacity: 1
                    };
                } else if (feature.geometry.type == "MultiPoint") {
                    return {
                        radius: 7,
                        fillColor: "#ffffff",
                        fillOpacity: 1,
                        color: "#d26d71",
                        weight: 5,
                        opacity: 1
                    };
                }
            }
        });
    };

    //$scope.addButtons = function() {

       /* var stopBusButton = L.easyButton({
            position: 'topright',
            leafletClasses: false,
            states: [{
                stateName: 'stop-bus-off',
                icon: '<img src="img/icons/bus-stop-off.png"/>',
                title: 'stop bus off',
                onClick: function(btn, map) {
                    $scope.route_map.objectParaderos.addTo($rootScope.mapRoute);
                    btn.state('stop-bus-on');
                }
            }, {
                stateName: 'stop-bus-on',
                icon: '<img src="img/icons/bus-stop-on.png"/>',
                title: 'stop bus on',
                onClick: function(btn, map) {
                    $rootScope.mapRoute.removeLayer($scope.route_map.objectParaderos);
                    btn.state('stop-bus-off');
                }
            }]
        });
        stopBusButton.addTo($rootScope.mapRoute);*/

       /*  var switchPublicidad = L.easyButton({
            position: 'topright',
            leafletClasses: false,
            states: [{
                    stateName: 'stop-bus-off',
                    icon: '<img style="margin-bottom: 10px;" src="img/icons/megafonoGris.png"/>',
                    title: 'stop bus off',
                    onClick: function(btn, map) {
                    $scope.getUbication();
                    }
                }]
            });
            switchPublicidad.addTo($rootScope.mapRoute);
        }*/

    $scope.getUbication = function()
    {
        console.log('obteniendo ubi');

          cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {    
            if(!enabled){
                alert("GPS esta " + (enabled ? "habilitado"  : "deshabilitado" + " para poder ofrecer una mejor experiencia de uso, solicitamos activarlo" ));
                cordova.plugins.diagnostic.switchToLocationSettings();
            }
            else
            {      
                navigator.geolocation.getCurrentPosition(function(pos) 
                {
                    console.log('obtiene ubicacion');
                    var lat = pos.coords.latitude
                    var lng = pos.coords.longitude
                    $scope.setUbication(lat, lng);
                    }, function(error) {
                        console.log('error');
                        var lat = 20.96707;
                        var lng = -89.623729;
                        $scope.setUbication(lat, lng);
                        $cordovaToast.show("No se pudo obtener la ubicación", "short", "bottom").then(function(success) {}, function(error) {});
                    }, {
                        enableHighAccuracy: true,
                }); 
            }
              }, function(error) {
                alert("The following error occurred: " + error);
            });


    }

    $scope.showErrorMessages = function(response) {
        var message = ""
        switch (response.status) {
            case 500:
                message = "Ocurrio un erro al intentar procesar su solicitud, intentelo de nuevo mas tarde o contacte al administrador del sistema";
                break;
            case 408:
            case 504:
                message = "El servidor tardo demasiado en responder";
                break;
            case 503:
                message = "El servicio no se encuentra disponible por el momento, intentelo de nuevo mas tarde o conctacte al administrador del sistema";
                break;
            case 204:
                message = "No se encontro la respuesta solicitada, intentelo nuevamente";
            default:
                message = "Ocurio un error consulte al administrador";

        }

        $cordovaToast.show(message, "long", "bottom").then(function(success) {}, function(error) {});
    }

    $rootScope.$on('$stateChangeSuccess',
        function(event, toState, toParams, fromState, fromParams) {


            if (fromState.name == "app.route_detail") {
                var markperBus = L.icon({
                    iconUrl: 'img/icons/bus.png',
                    iconSize: [40, 40]
                });
                $rootScope.mapRoute._onResize();
                $rootScope.abConnection.subscribe(JSON.stringify({
                    ruta_id: $scope.route.id_ruta
                }), function(data, response) {
                    console.log(response);
                    var markers = JSON.parse(response);
                    if ($scope.points.length > 0) {
                        angular.forEach($scope.points, function(value, key) {
                            $rootScope.mapRoute.removeLayer(value);
                        });
                        $scope.points = [];
                    }
                    if (markers.length > 0) {
                        $scope.points = [];
                        angular.forEach(markers, function(value, key) {
                            var marker = L.marker([value.lat, value.lng], {
                                icon: markperBus
                            });
                            $scope.points.push(marker);
                            marker.addTo($rootScope.mapRoute);
                        });
                    }
                });
            }

            if (toState.name == "app.results" || toState.name == "app.routes") {
                $rootScope.abConnection.close();
            }

        });

    angular.element(document).ready(function() {
        $scope.init();
    });

});